using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Hangfire;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StackExchange.Redis;

namespace MyHangfire.Web
{
    public class Startup
    {
        public static ConnectionMultiplexer Redis { get; private set; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            Redis = ConnectionMultiplexer.ConnectAsync($"localhost,defaultDatabase=0").GetAwaiter().GetResult();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            services.AddSingleton<IConnectionMultiplexer>(Redis);

            services.AddHangfire(configuration =>
            {
                configuration.UseRedisStorage(Redis, new Hangfire.Redis.RedisStorageOptions
                {
                    Prefix = "TLS_HF_"
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
            }

            app.UseStaticFiles();

            app.UseHangfireDashboard();

            app.UseMvc();
        }
    }
}
