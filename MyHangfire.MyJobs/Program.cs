﻿using Hangfire;
using Microsoft.Extensions.DependencyInjection;
using MyHangfire.Jobs;
using StackExchange.Redis;
using System;

namespace MyHangfire.MyJobs
{
    class Program
    {
        static void Main(string[] args)
        {
            var backgrounderMachine = new BackgrounderMachine();

            backgrounderMachine.Run();
        }
    }

    public class BackgrounderMachine
    {
        private IServiceProvider _ServiceProvider;
        private object ServiceProviderSynchron = new object();

        public TimeZoneInfo BackgrounderTimeZoneInfo => TimeZoneInfo.Local;
        
        /// <summary>
        /// Thread-Safe instance of internal Backgrounder app service resolver.
        /// </summary>
        public IServiceProvider ServiceProvider
        {
            get
            {
                if (_ServiceProvider == null)
                {
                    // Not sure if you really have to make this thread-safe. 
                    lock (ServiceProviderSynchron)
                    {
                        if (_ServiceProvider == null)
                        {
                            _ServiceProvider = this.ConfigureServices();
                        }

                    }
                }

                return _ServiceProvider;
            }
        }

        public IServiceProvider ConfigureServices()
        {
            var services = new ServiceCollection();

            var redis = ConnectionMultiplexer.Connect($"localhost,defaultDatabase=0");
            services.AddSingleton<IConnectionMultiplexer>(redis);

            services.AddHangfire(hf =>
            {
                hf.UseRedisStorage(redis, new Hangfire.Redis.RedisStorageOptions
                {
                    Prefix = "TLS_HF_",
                });
            });

            services.AddTransient<BackgroundJobServer>();
            services.AddTransient<JobService>();

            var serviceProvider = services.BuildServiceProvider();

            return serviceProvider;
        }

        public void Run()
        {
            var backgrounderJobServer = this.ServiceProvider.GetRequiredService<BackgroundJobServer>();
            var jobService = this.ServiceProvider.GetRequiredService<JobService>();

            Console.WriteLine("Backgrounder has started");

            // Register your jobs here.
            RecurringJob.AddOrUpdate("Print Hello World", () => jobService.PrintHelloWorld(), "* * * * *", this.BackgrounderTimeZoneInfo);

            // Console.ReadKey() will prevent the instant shutdown of the application.
            // Reference: https://docs.hangfire.io/en/latest/background-processing/processing-jobs-in-console-app.html.
            Console.ReadKey();
        }
    }
}
